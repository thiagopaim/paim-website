<?php
get_header(); ?>

	<main>
		<header>
			<?php 
			the_title('<h1>', '</h1>'); ?>
		</header>
		<?php 
		if(have_posts()) :
			while(have_posts()) : the_post();
				the_content();
			endwhile;
		endif; ?>
	</main>

<?php 
get_footer(); ?>