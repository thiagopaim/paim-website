<?php
get_header();

	get_template_part('templates/home/hero');
	get_template_part('templates/home/projects');
	get_template_part('templates/home/about');

get_footer(); ?>