<?php 
/* Page Name: Category */ ?>

<?php
get_header(); 

	echo '<div class="breadcrumb"><div class="row">';
		bcn_display();
	echo '</div></div>';

	$category = get_category( get_query_var( 'cat' ) );
	$image = get_field('cat_header', 'category_'. $category->term_id .'');
	
	if($image) :
		$headerClass = 'has-image';
	else :
		$headerClass = '';
	endif;

	echo '<header class="' . $headerClass . '" style="background-image: url(' . $image['sizes']['slider'] . ')">';
		echo '<div class="slider__overlay"></div>';
		echo '<div class="row">';
			echo '<h1 class="category__title">Produtos <span>' . get_the_archive_title() . '</span></h1>';
		echo '</div>';
	echo '</header>'; ?>
	
	<section class="row">
		<div class="page__content">
			<?php	
			$args = array( 'post_type' => 'produto', 'cat' => $category->term_id );
			$product = new WP_Query($args);
			if($product->have_posts()) :
				echo '<div class="products">';
					while($product->have_posts()) : $product->the_post();
						get_template_part('templates/loop/resume');
					endwhile;
				echo '</div>';
			endif;
			wp_pagenavi();
			wp_reset_postdata(); ?>
		</div>
	</section>

<?php 
get_footer(); ?>