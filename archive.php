<?php 
/* Page Name: Archive */ ?>

<?php
get_header(); 

	echo '<div class="breadcrumb"><div class="row">';
		bcn_display();
	echo '</div></div>'; ?>
	
	<section class="row">
		<div class="page__content">
			<?php	
			echo '<header>';
				echo '<h1 class="page__title">Produtos</h1>';
			echo '</header>';
			if(have_posts()) :
				echo '<div class="products">';
					while(have_posts()) : the_post();
						get_template_part('templates/loop/resume');
					endwhile;
				echo '</div>';
			endif;
			wp_pagenavi(); ?>
		</div>
	</section>

<?php 
get_footer(); ?>