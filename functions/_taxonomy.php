<?php
// customs taxonomies
// ------------------------------------- //
// function custom_taxonomy() {

// 	$labels = array(
// 		'name'                       => _x( 'Categorias', 'Taxonomy General Name', 'text_domain' ),
// 		'singular_name'              => _x( 'Categorias', 'Taxonomy Singular Name', 'text_domain' ),
// 		'menu_name'                  => __( 'Categorias', 'text_domain' ),
// 		'all_items'                  => __( 'Listar todas', 'text_domain' ),
// 		'parent_item'                => __( 'Categoria', 'text_domain' ),
// 		'parent_item_colon'          => __( 'Categoria', 'text_domain' ),
// 		'new_item_name'              => __( 'Nova Categoria', 'text_domain' ),
// 		'add_new_item'               => __( 'Adicionar Categoria', 'text_domain' ),
// 		'edit_item'                  => __( 'Editar Categoria', 'text_domain' ),
// 		'update_item'                => __( 'Atualizar Categoria', 'text_domain' ),
// 		'view_item'                  => __( 'Ver Categoria', 'text_domain' ),
// 		'separate_items_with_commas' => __( 'Separe os itens por virgula \" , \"', 'text_domain' ),
// 		'add_or_remove_items'        => __( 'Adicionar ou Remover Itens', 'text_domain' ),
// 		'choose_from_most_used'      => __( 'Escolha entre as mais usadas', 'text_domain' ),
// 		'popular_items'              => __( 'Categorias Populares', 'text_domain' ),
// 		'search_items'               => __( 'Buscar Categoria', 'text_domain' ),
// 		'not_found'                  => __( 'Nenhum item cadastrado...', 'text_domain' ),
// 		'no_terms'                   => __( 'Nenhum item cadastrado...', 'text_domain' ),
// 		'items_list'                 => __( 'Lista de itens', 'text_domain' ),
// 		'items_list_navigation'      => __( 'Navegação dos itens', 'text_domain' ),
// 	);
// 	$args = array(
// 		'labels'                     => $labels,
// 		'hierarchical'               => true,
// 		'public'                     => true,
// 		'show_ui'                    => true,
// 		'show_admin_column'          => true,
// 		'show_in_nav_menus'          => true,
// 		'show_tagcloud'              => true,
// 	);
// 	register_taxonomy( 'catproduto', array( 'produto' ), $args );

// }
// add_action( 'init', 'custom_taxonomy', 0 );