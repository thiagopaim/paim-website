<?php 
// charachters limit
// -------------------------------- //
function excerpt($limit) {
	$excerpt = explode(' ', get_the_excerpt(), $limit);
	if (count($excerpt)>=$limit) :
		array_pop($excerpt);
		$excerpt = implode(" ",$excerpt).'...';
	else :
		$excerpt = implode(" ",$excerpt);
	endif;
	
	$excerpt = preg_replace('`\[[^\]]*\]`','',$excerpt);
	return $excerpt;
}