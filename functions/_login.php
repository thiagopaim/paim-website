<?php
function css_login() { ?>

    <style type="text/css">
    	body.login { background: #e5e5e5; }
        body.login h1 a { display: none; }
    </style>

<?php }
add_action( 'login_enqueue_scripts', 'css_login' );