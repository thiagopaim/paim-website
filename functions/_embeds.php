<?php
// responsive embed
// ------------------------------------- //
function embed_html( $html ) {
    return '<div class="flex-video widescreen">' . $html . '</div>';
}
add_filter( 'embed_oembed_html', 'embed_html', 10, 3 );
add_filter( 'video_embed_html', 'embed_html' ); // Jetpack