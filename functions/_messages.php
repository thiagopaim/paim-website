<?php

// notices
add_action('admin_notices', 'showAdminMessages');

function showAdminMessages() {
	$messages = array();

	include_once( ABSPATH . 'wp-admin/includes/plugin.php' );

	// Download Advanced Custom Fields
	if( !is_plugin_active( 'advanced-custom-fields-pro/acf.php' ) ) :
		$messages[] = 'Este tema precisa do plugin <strong>Advanced Custom Fields Pro</strong>. <a href="https://www.advancedcustomfields.com/pro/">Clique aqui e saiba mais</a>.';
	endif;

	// Message for Google Analyticts, man!
	// if( !get_field('analyticsCode', 'option') ) :
	// 	$messages[] = 'Não se esqueça de adicionar o código do Google Analytics. <a href="admin.php?page=acf-options-opcoes">Clique aqui e adicione já!</a>';
	// endif;

	if(count($messages) > 0) :
		echo '<div id="message" class="error">';
			foreach($messages as $message) :
				echo '<p>' . $message . '</p>';
			endforeach;
		echo '</div>';
	endif;
}

// mensagem de bem-vindo
function custom_dashboard() {
	$screen = get_current_screen();
	if( $screen->base == 'dashboard' ) {
		include '_welcome.php';
	}
}
add_action('admin_notices', 'custom_dashboard');

