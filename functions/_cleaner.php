<?php 
// clean header
// ------------------------------------- //
function cleanUp() {
	remove_action('wp_head', 'wp_generator');                			
	remove_action('wp_head', 'wlwmanifest_link');            			
	remove_action('wp_head', 'rsd_link');                    			
	remove_action('wp_head', 'wp_shortlink_wp_head');   

	remove_action('wp_head', 'wp_resource_hints', 2);

	remove_action('wp_head', 'adjacent_posts_rel_link_wp_head', 10);   

	add_filter('the_generator', '__return_false');            		
	add_filter('show_admin_bar','__return_false');            		

	remove_action( 'wp_head', 'print_emoji_detection_script', 7 );  
	remove_action( 'wp_print_styles', 'print_emoji_styles' );
}
add_action('after_setup_theme', 'cleanUp');

// clean widgets
add_action( 'widgets_init', 'remove_recent_comments_style' );
function remove_recent_comments_style() {
	global $wp_widget_factory;
	remove_action( 'wp_head', array( $wp_widget_factory->widgets['WP_Widget_Recent_Comments'], 'recent_comments_style'  ) );
}

// clear footer
// ------------------------------------- //
function cleanDown(){
	wp_deregister_script( 'wp-embed' );
} add_action( 'wp_footer', 'cleanDown' );


// remove WordPress version
// ------------------------------------- //
remove_action('wp_head', 'wp_generator');