<?php
// Register Custom Post Type
// Register Custom Post Type
function custom_post_type() {

	$labels = array(
		'name'                  => _x( 'Projetos', 'Post Type General Name', 'text_domain' ),
		'singular_name'         => _x( 'Projeto', 'Post Type Singular Name', 'text_domain' ),
		'menu_name'             => __( 'Projetos', 'text_domain' ),
		'name_admin_bar'        => __( 'projetos', 'text_domain' ),
		'archives'              => __( 'Projetos', 'text_domain' ),
		'attributes'            => __( 'Projetos', 'text_domain' ),
		'parent_item_colon'     => __( 'Projeto:', 'text_domain' ),
		'all_items'             => __( 'Projetos', 'text_domain' ),
		'add_new_item'          => __( 'Adicionar projeto', 'text_domain' ),
		'add_new'               => __( 'Adicionar novo', 'text_domain' ),
		'new_item'              => __( 'Novo projeto', 'text_domain' ),
		'edit_item'             => __( 'Editar projeto', 'text_domain' ),
		'update_item'           => __( 'Atualizar projeto', 'text_domain' ),
		'view_item'             => __( 'Ver projeto', 'text_domain' ),
		'view_items'            => __( 'Ver projetos', 'text_domain' ),
		'search_items'          => __( 'Buscar projetos', 'text_domain' ),
		'not_found'             => __( 'Nada encontrado', 'text_domain' ),
		'not_found_in_trash'    => __( 'Nada encontrado na lixeira', 'text_domain' ),
		'featured_image'        => __( 'Imagem de destaque', 'text_domain' ),
		'set_featured_image'    => __( 'Selecionar imagem', 'text_domain' ),
		'remove_featured_image' => __( 'Remover imagem', 'text_domain' ),
		'use_featured_image'    => __( 'Usar imagem', 'text_domain' ),
		'insert_into_item'      => __( 'Inserir no projeto', 'text_domain' ),
		'uploaded_to_this_item' => __( 'Enviar para este projeto', 'text_domain' ),
		'items_list'            => __( 'Projetos da lista', 'text_domain' ),
		'items_list_navigation' => __( 'Lista de projetos', 'text_domain' ),
		'filter_items_list'     => __( 'Filtar projetos', 'text_domain' ),
	);
	$args = array(
		'label'                 => __( 'Projeto', 'text_domain' ),
		'description'           => __( 'Área para adição dos projetos realizados', 'text_domain' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'category' ),
		'hierarchical'          => true,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'show_in_rest'			=> true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'post',
	);
	register_post_type( 'projetos', $args );

}
add_action( 'init', 'custom_post_type', 0 );