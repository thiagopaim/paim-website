<?php
// support to thumbnails
// ------------------------------------- //
add_theme_support( 'post-thumbnails' );

// custom image size
add_image_size( 'Featured', 			1620, 830, true );
add_image_size( 'Projects',  			775, 360, true );
add_image_size( 'Projects Medium',  	575, 360, true );
add_image_size( 'Projects Thumbnail',  	375, 360, true );
add_image_size( 'Gallery',  			775, 9999, true );