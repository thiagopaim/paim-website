<?php
// options
// ------------------------------------- //
if( function_exists('acf_add_options_page') ) {
	acf_add_options_page(array(
		'page_title'	=> 'Informações',
		'menu_title'	=> 'Informações',
		'menu_slug'		=> 'info',
		'capability'	=> 'edit_posts',
		'redirect'		=> true,
		'icon_url'		=> 'dashicons-info',
		'position' 		=> '2'
	));	
}