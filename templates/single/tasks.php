<?php 
$tasks 		= get_field('project_tasks');
$technical 	= get_field('project_technical'); ?>

<section class="post__tasks">
	<div class="post__wrapper--task">
		<?php 
		if($tasks) : 
			echo '<ul class="post__task-list">';
			foreach ($tasks as $task) :
			 	
				echo '<li class="post__task-item post__task-item--' . $task['value'] . '"><span class="post__task-icon"></span>' . $task['label'] . '</li>';

			endforeach;
			echo '</ul>';
		endif;

		echo $technical; ?>
	</div>
</section>