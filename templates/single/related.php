<section class="area related">
	<h1 class="area__title">Outros <span>projetos</span></h1>	
	<?php 
	$count = 0;
	$args = array( 'post_type' => 'projetos', 'posts_per_page' => 3, 'orderby' => 'rand' );
	$projects = new WP_Query( $args );

	echo '<div class="area__wrapper">';
		while ( $projects->have_posts() ) : $projects->the_post();

			$image 		 = get_field('project_list_image');
			$category	 = get_field('project_type');
			$title 		 = get_the_title();
			$link 		 = get_the_permalink();
			
			$imageLarge  = $image['sizes']['Projects'];
			$imageMedium = $image['sizes']['Projects Medium'];
			$imageThumb  = $image['sizes']['Projects Thumbnail'];

			
			echo '<a href="' . $link . '" title="' . $title . '" class="area__post" style="background-image: url('. $imageThumb .')">';
				echo '<div class="overlay"></div>';
				echo '<div class="area__post-text">';
					echo '<span class="area__post-meta">' . $category . '</span>';
					echo '<h2 class="area__post-title">' . $title . '</h2>';
				echo '</div>';
			echo '</a>';

			$count++;
		endwhile;
	echo '</div>'; ?>
</section>