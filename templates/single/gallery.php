<?php 
$gallery = get_field('project_gallery');
if($gallery) : ?>

	<section class="post__gallery">
		<?php 
		echo '<ul class="post__gallery-list">';
			foreach ($gallery as $key => $image) :
				$imageData = wp_get_attachment_caption( $image['id'] );
			 	echo '<li class="post__gallery-item">';
			 		echo '<figure>';
			 			echo '<img src="' . $image['sizes']['medium_large'] . '" alt="Galeria de imagens do projeto' . get_the_title() . ': Imagem número ' . $key . '" class="post__gallery-image">';
			 			if($imageData) echo '<figcaption>' . $imageData . '</figcaption>';
			 		echo '</figure>';
			 	echo '</li>';
			endforeach;
		echo '</ul>'; ?>
	</section>

<?php
endif; ?>