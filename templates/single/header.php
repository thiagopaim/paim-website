<?php 
$image 		 = get_field('project_image');
$category	 = get_field('project_type');
$about 		 = get_field('project_about');
$link 		 = get_field('project_link');
$title 		 = get_the_title(); ?>

<header class="post__header" style="background-image: url(<?php echo $image['sizes']['Featured']; ?>)">
	<?php 
	echo '<div class="post__wrapper">';
		echo '<div class="post__text">';
			echo '<span class="post__meta">' . $category . '</span>';
			echo '<h1 class="post__title">' . $title . '</h1>';
			echo '<p class="post__paragraph">' . $about . '</p>';
			if($link) echo '<a class="button" href="' . $link . '" title="Ir para ' . $title . '" target="_blank">Ir para ' . $category . '</a>';
		echo '</div>';
	echo '</div>'; ?>
</header>