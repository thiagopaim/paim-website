<?php 
$image = get_field('featured_image'); ?>

<section class="header" style="background-image:url(<?php echo $image['sizes']['Featured']; ?>);">
	<div class="hero" id="hero">
		<h1 class="hero__title" data-timer="1500" data-type='[ "websites", "hotsites", "landing pages", "lojas virtuais", "aplicativos" ]'>
			Faço <span class="hero__typing">websites </span><span class="hero__block">bem legais e </span><span class="hero__block">fáceis de usar </span>
		</h1>
		<p>Para quem liga para formação, possuo pós-graduado em Design de Interação, MBA em Marketing Digital, Graduação em Desenho Industrial com ênfase em Programação Visual.</p>
	</div>
</section>