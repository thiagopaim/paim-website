<section class="area about" id="about">
	<h1 class="area__title">Sou o <span>Paim</span></h1>
	<div class="area__wrapper--about">
		<?php 
		the_field('about', 2); ?>
	</div>
</section>