<nav class="nav">
	<div class="nav__wrapper">
		<a href="<?php echo get_bloginfo('url'); ?>" class="nav__logo">Thiago Paim - Website</a>
		<?php 
		if(is_page('home')) :
			display_main_menu();
		else: 
			display_external_menu();
		endif; ?>
	</div>
</nav>