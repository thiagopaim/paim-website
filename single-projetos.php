<?php
get_header(); ?>

<main class="post">
	<?php 
	get_template_part('templates/single/header');
	get_template_part('templates/single/tasks');
	get_template_part('templates/single/gallery');
	get_template_part('templates/single/related'); ?>
</main>
	
<?php 
get_footer(); ?>