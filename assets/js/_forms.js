let inputs = document.querySelectorAll('.wpcf7-form-control')

function focusOnInput(element, index, array) {
	element.addEventListener('focus', function(){
		this.parentElement.parentElement.classList.add('is-active')
	})

	element.addEventListener('blur', function(){
		this.parentElement.parentElement.classList.remove('is-active')

		if(element.value != '' || this.value != this.defaultValue) {
			this.parentElement.parentElement.classList.add('has-value')
		}
	})
}

// inputs.forEach(focusOnInput);

for (var j = 0; j < inputs.length; j++) {
	focusOnInput(inputs[j])
}