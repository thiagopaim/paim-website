let menu = document.querySelectorAll('#menu-principal .nav__link')

if(menu.length != 0) {
	menu[0].classList.add('is-active')
}

// menu.forEach(function(menuItem) {
// 	menuItem.addEventListener('click', function(event) {
// 		event.preventDefault()
// 		checkClass()
// 		let linkId = this.href.split('#')[1]
// 		if(linkId) {
// 			let element = document.querySelector('#' + linkId)
// 			scrollTo(element)

// 			if(element.id === linkId) {
// 				this.classList.add('is-active')
// 			}
// 		}
// 	})
// })

for (var i = 0; i < menu.length; i++) {
	menu[i].addEventListener('click', function(event) {
		if (/MSIE 10/i.test(navigator.userAgent)) {
			// console.log('IE...')
		} else {
			event.preventDefault()
		}
		checkClass()
		let linkId = this.href.split('#')[1]
		if(linkId) {
			let section = document.querySelector('#' + linkId)
			scrollTo(section)

			if(section.id === linkId) {
				this.classList.add('is-active')
			}
		}
	})
}

function scrollTo(section) {
	// console.log(section.id)
	window.scroll({
		behavior: 'smooth',
		left: 0,
		top: section.offsetTop
	})
}

function checkClass() {
	let activeMenu = document.querySelector('#menu-principal .is-active')
	if(activeMenu != null &&  activeMenu.classList.contains('is-active')) {
		activeMenu.classList.remove('is-active')
	}
}
