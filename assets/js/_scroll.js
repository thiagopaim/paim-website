let scrollPosition,
	menu = document.querySelector('.nav')

window.addEventListener('scroll', browserWasScrolled)

function browserWasScrolled(){
	scrollPosition = typeof window.scrollY === 'undefined' ? window.pageYOffset : window.scrollY;
	if(scrollPosition >= 150){
		menu.classList.add('is-active')
	} else {
		menu.classList.remove('is-active')
	}
}