<?php
get_header(); ?>

	<main>
		<header>
			<h1>Erro 404</h1>
		</header>
		<?php 
		if(have_posts()) :
			while(have_posts()) : the_post();
				the_content();
			endwhile;
		endif; ?>
	</main>
	
<?php 
get_footer(); ?>