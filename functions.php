<?php
/* admin
------------------------------------------------- */
require_once('functions/_admin.php');

/* clear wordpress scripts from header / footer
------------------------------------------------- */
require_once('functions/_cleaner.php');

/* contact form 7
------------------------------------------------- */
require_once('functions/_contact.php');

/* dashboard
------------------------------------------------- */
require_once('functions/_dashboard.php');

/* responsive embeds
------------------------------------------------- */
require_once('functions/_embeds.php');

/* custom images size
------------------------------------------------- */
require_once('functions/_images.php');

/* login
------------------------------------------------- */
require_once('functions/_login.php');

/* maps
------------------------------------------------- */
require_once('functions/_maps.php');

/* menu
------------------------------------------------- */
require_once('functions/_menu.php');

/* custom messages
------------------------------------------------- */
require_once('functions/_messages.php');

/* options
------------------------------------------------- */
require_once('functions/_options.php');

/* custom pages
------------------------------------------------- */
require_once('functions/_pages.php');

/* custom posts
------------------------------------------------- */
require_once('functions/_posts.php');

/* custom posts
------------------------------------------------- */
require_once('functions/_rights.php');

/* sidebars
------------------------------------------------- */
require_once('functions/_sidebar.php');

/* custom taxonomy
------------------------------------------------- */
require_once('functions/_taxonomy.php');

/* text limit
------------------------------------------------- */
require_once('functions/_text.php');

// -------------------------------------- this is all folks -------------------------------------- \\
